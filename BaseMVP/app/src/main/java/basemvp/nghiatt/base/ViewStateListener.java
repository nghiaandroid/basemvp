package basemvp.nghiatt.base;

public interface ViewStateListener {
    void onViewAppear();

    void onViewDisappear();
}
