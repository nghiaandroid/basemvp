package basemvp.nghiatt.base.mvp;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class BasePresenter<T extends BaseViewImpl> implements BasePresenterImpl {
    private WeakReference<T> view;

    void setView(BaseViewImpl view) {
        this.view = new WeakReference<>((T) view);
    }

    protected T getView() {
        return view.get();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    static Object initialize(Class<?> clazz) {
        try {
            Type genericSuperClass = clazz.getGenericSuperclass();
            Type genericType = ((ParameterizedType) genericSuperClass).getActualTypeArguments()[0];
            try {
                return (Class.forName( genericType.toString().replace("class ","")).newInstance());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        Log.e("AA", "bb");
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onViewAppear() {

    }

    @Override
    public void onViewDisAppear() {

    }
}
