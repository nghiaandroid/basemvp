package basemvp.nghiatt.base.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Preconditions;

import basemvp.nghiatt.base.BaseFragment;

public abstract class MVPFragment<T extends BasePresenter> extends BaseFragment implements BaseViewImpl {
    private T mPresenter;

    public T getPresenter() {
        return mPresenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (getView() == null) {
            //noinspection unchecked
            mPresenter = (T) BasePresenter.initialize(getClass());
            Preconditions.checkNotNull(mPresenter).setView(this);
            mPresenter.onCreate();
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onViewAppear();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.onViewDisAppear();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.onDestroy();
    }
}
