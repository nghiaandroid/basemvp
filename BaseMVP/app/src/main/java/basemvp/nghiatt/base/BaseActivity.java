package basemvp.nghiatt.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements ViewStateListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        onViewAppear();
        Log.e("State", "onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        onViewDisappear();
    }

    protected abstract int getLayoutResource();
}
