package basemvp.nghiatt.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment implements ViewStateListener {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getView();
        if (view == null) {
            view = inflater.inflate(getLayoutResource(), container, false);
        }
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        onViewAppear();
    }

    @Override
    public void onPause() {
        super.onPause();
        onViewDisappear();
    }

    protected abstract int getLayoutResource();
}
