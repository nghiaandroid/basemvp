package basemvp.nghiatt.base.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.common.base.Preconditions;

import basemvp.nghiatt.base.BaseActivity;

public abstract class MVPActivity<T extends BasePresenter> extends BaseActivity implements BaseViewImpl {
    private T mPresenter;

    public T getPresenter() {
        return mPresenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //noinspection unchecked
        mPresenter = (T) BasePresenter.initialize(getClass());
        Preconditions.checkNotNull(mPresenter).setView(this);
        mPresenter.onCreate();
    }

    @Override
    public void onViewAppear() {
        mPresenter.onViewAppear();
    }

    @Override
    public void onViewDisappear() {
        mPresenter.onViewDisAppear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
