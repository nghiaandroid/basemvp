package basemvp.nghiatt.base.mvp;

public interface BasePresenterImpl {
    void onCreate();

    void onViewAppear();

    void onViewDisAppear();

    void onDestroy();
}
