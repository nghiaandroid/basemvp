package basemvp.nghiatt.home;

import android.graphics.Color;

import java.util.Random;

import basemvp.nghiatt.base.mvp.BasePresenter;

class HomePresenter extends BasePresenter<HomeContract.ViewImpl> implements HomeContract.Presenter {

    @Override
    public void updateBackGround() {
        Random rand = new Random();
        int r = rand.nextInt(255);
        int g = rand.nextInt(255);
        int b = rand.nextInt(255);
        getView().setBackGroundColor(Color.rgb(r, g, b));
    }

    @Override
    public void updateText() {
        getView().setText(System.currentTimeMillis() + "");
    }
}
