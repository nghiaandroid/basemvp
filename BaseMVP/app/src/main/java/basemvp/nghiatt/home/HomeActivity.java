package basemvp.nghiatt.home;

import android.widget.TextView;
import android.widget.Toast;

import basemvp.dev.nghiatt.basemvp.R;
import basemvp.nghiatt.base.mvp.MVPActivity;
import butterknife.BindView;
import butterknife.OnClick;

public class HomeActivity extends MVPActivity<HomePresenter> implements HomeContract.ViewImpl {
    @BindView(R.id.tvTest)
    TextView tvTest;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @OnClick(R.id.btnChangeBackground)
    void onBtnChangeBackgroundClick() {
        getPresenter().updateBackGround();
    }

    @OnClick(R.id.btnChangeText)
    void onBtnUpdateTextClick() {
        getPresenter().updateText();
    }

    @Override
    public void setText(String text) {
        tvTest.setText(text);
    }

    @Override
    public void setBackGroundColor(int color) {
        getWindow().getDecorView().getRootView().setBackgroundColor(color);
    }

    @Override
    public void onViewAppear() {
        Toast.makeText(this, "onViewAppear", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewDisappear() {
        Toast.makeText(this, "onViewDisappear", Toast.LENGTH_SHORT).show();
    }
}
