package basemvp.nghiatt.home;

import basemvp.nghiatt.base.mvp.BasePresenterImpl;
import basemvp.nghiatt.base.mvp.BaseViewImpl;

interface HomeContract {
    interface ViewImpl extends BaseViewImpl {
        void setText(String text);

        void setBackGroundColor(int color);
    }

    interface Presenter extends BasePresenterImpl {
        void updateBackGround();

        void updateText();
    }
}
